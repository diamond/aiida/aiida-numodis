#! usr/bin/env python
"""
AiiDA plugin for ``NUMODIS``.
"""

__version__= "0.1.0"


import aiida_numodis.calculations
import aiida_numodis.cli
import aiida_numodis.data
import aiida_numodis.parsers
import aiida_numodis.workflows
import aiida_numodis.utils
