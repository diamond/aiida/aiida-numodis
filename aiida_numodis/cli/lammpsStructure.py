"""Command line utilities to create and inspect `LammpsStructureFiles` nodes."""
from aiida.cmdline.utils import decorators, echo
from aiida.cmdline.commands.cmd_data import verdi_data
from aiida.cmdline.params import arguments, types
import click
import pathlib

@verdi_data.group('numodis.lammpsstructure')
def cmd_lammpsstruct():
    """From `LammpsStructureFiles` nodes, read and extract data."""

@cmd_lammpsstruct.command('list')
@arguments.NODE(type=types.DataParamType(sub_classes=('aiida.data:numodis.lammpsstruct',)))
@decorators.with_dbenv()
def cmd_list_files(node):
    """List all stored files."""
    echo.echo('Listing the files and directories contained in the node '+str(node.pk)+':')
    object_list=node.base.repository.list_objects()
    for object in object_list:
        echo.echo(object.name)

@cmd_lammpsstruct.command('content')
@arguments.NODE(type=types.DataParamType(sub_classes=('aiida.data:numodis.lammpsstruct',)))
@click.argument('filename', type=click.Path())
@decorators.with_dbenv()
def cmd_get_content(node, filename):
    """Display content of a single stored file.
    """
    try:
        echo.echo(node.get_file(filename).read().decode())
    except:
        echo.echo(str(filename)+' file not found. To see the list of stored files try the command:')
        echo.echo('verdi data numodis.lammpsstruct list '+str(node.pk))

@cmd_lammpsstruct.command('extract')
@arguments.NODE(type=types.DataParamType(sub_classes=('aiida.data:numodis.lammpsstruct',)))
@click.argument('target_dir', type=click.Path(resolve_path=True))
def cmd_extract(node,target_dir):
    """Copy all stored files to a local directory."""
    directory=pathlib.Path(target_dir).resolve()
    directory.mkdir(parents=True, exist_ok=True)
    object_list=node.base.repository.list_objects()
    for object in object_list:
        filename=pathlib.Path(directory,object.name)
        with node.get_file(object.name) as content:
            with open(filename,'wb') as f:
                f.write(content.read())