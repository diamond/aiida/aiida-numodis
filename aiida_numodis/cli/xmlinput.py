"""Command line utilities to create and inspect `xmlinput` nodes."""
from aiida.cmdline.utils import decorators, echo
from aiida.cmdline.commands.cmd_data import verdi_data
from aiida.cmdline.params import arguments, types
import click


@verdi_data.group('numodis.xmlinput')
def cmd_xmlinput():
    """From `NumodisXmlData` nodes, read and extract data."""

@cmd_xmlinput.command('list')
@arguments.NODE(type=types.DataParamType(sub_classes=('aiida.data:numodis.xmlinput',)))
@decorators.with_dbenv()
def cmd_list_files(node):
    """List all stored files and directories."""
    echo.echo('Listing the files and directories contained in the node '+str(node.pk)+':')
    object_glob=node.glob()
    for object in object_glob:
        echo.echo(object)

@cmd_xmlinput.command('content')
@arguments.NODE(type=types.DataParamType(sub_classes=('aiida.data:numodis.xmlinput',)))
@click.argument('filename', type=click.Path())
@decorators.with_dbenv()
def cmd_get_content(node, filename):
    """Display content of a single stored file.
    """
    try:
        echo.echo(node.get_object_content(filename))
    except:
        echo.echo(str(filename)+' file not found. To see the list of stored files try the command:')
        echo.echo('verdi data numodis.xmlinput list '+str(node.pk))

@cmd_xmlinput.command('extract')
@arguments.NODE(type=types.DataParamType(sub_classes=('aiida.data:numodis.xmlinput',)))
@click.argument('target_dir', type=click.Path(resolve_path=True))
def cmd_extract(node,target_dir):
    """Copy all stored files to a local directory."""
    node.copy_tree(target=target_dir)