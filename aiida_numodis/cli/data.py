"""Command line utilities to inspect `Data` nodes."""
from aiida.cmdline.utils import decorators, echo
from aiida.cmdline.commands.cmd_data import verdi_data
from aiida.cmdline.params import arguments
import click


@verdi_data.group('diamond.data')
def cmd_datafiles():
    """From `Data` nodes, read and extract data."""

@cmd_datafiles.command('list')
@arguments.NODE()
@decorators.with_dbenv()
def cmd_list_files(node):
    """List all stored files and directories."""
    echo.echo('Listing the files and directories contained in the node '+str(node.pk)+':')
    object_list=node.base.repository.list_objects()
    for object in object_list:
        echo.echo(object)

@cmd_datafiles.command('content')
@arguments.NODE()
@click.argument('filename', type=click.Path())
@decorators.with_dbenv()
def cmd_get_content(node, filename):
    """Display content of a single stored file.
    """
    try:
        echo.echo(node.get_object_content(filename))
    except:
        echo.echo(str(filename)+' file not found. To see the list of stored files try the command:')
        echo.echo('verdi data numodis.vtkfiles list '+str(node.pk))

@cmd_datafiles.command('extract')
@arguments.NODE()
@click.argument('target_dir', type=click.Path(resolve_path=True))
def cmd_extract(node,target_dir):
    """Copy all stored files to a local directory."""
    node.base.repository.copy_tree(target=target_dir)