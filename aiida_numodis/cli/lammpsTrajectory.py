"""Command line utilities to inspect `LammpsTrajectory` nodes."""
from aiida.cmdline.utils import decorators, echo
from aiida.cmdline.commands.cmd_data import verdi_data
from aiida.cmdline.params import arguments, types
import click
import tempfile

@verdi_data.group('diamond.lammpstrajectory')
def cmd_lammpstrajectory():
    """From `LammpsTrajectory` nodes, read and extract data."""

@cmd_lammpstrajectory.command('content')
@arguments.NODE(type=types.DataParamType(sub_classes=('aiida.data:lammps.trajectory',)))
@decorators.with_dbenv()
def cmd_get_content(node):
    """Display content of a single stored file.
    """
    temp_handle=tempfile.NamedTemporaryFile()
    node.write_as_lammps(temp_handle)
    temp_handle.flush()
    temp_handle.seek(0)
    echo.echo(str(temp_handle.read().decode()))

@cmd_lammpstrajectory.command('extract')
@arguments.NODE(type=types.DataParamType(sub_classes=('aiida.data:lammps.trajectory',)))
@click.argument('target_dir', type=click.Path(resolve_path=True))
def cmd_extract(node,target_dir):
    """Copy all stored files to a local directory."""
    node.base.repository.copy_tree(target=target_dir)