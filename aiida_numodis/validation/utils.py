import os
import json
import jsonschema

def validate_against_schema(yaml_dict):
    _file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "workflow_parameters.json",
        )
    with open(_file, encoding="utf8") as handler:
        schema = json.load(handler)

    jsonschema.validate(schema=schema, instance=yaml_dict)