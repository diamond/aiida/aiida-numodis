from math import sqrt
import os
import json

_DEFAULT_EXTRA={
    'restart_line':''
}

_DEFAULT_EMASS={
    "Ac": 227.0278,
    "Ag": 107.8682,
    "Al": 26.981539,
    "Am": 243.0614,
    "Ar": 39.948,
    "As": 74.92159,
    "At": 209.9871,
    "Au": 196.96654,
    "B":  10.811,
    "Ba": 137.327,
    "Be": 9.012182,
    "Bh": 262.,
    "Bi": 208.98037,
    "Bk": 247.0703,
    "Br": 79.904,
    "C" : 12.011,
    "Ca": 40.078,
    "Cd": 112.411,
    "Ce": 140.115,
    "Cf": 251.0796,
    "Cl": 35.4527,
    "Cm": 247.0703,
    "Cn": 277.,
    "Co": 58.9332,
    "Cr": 51.9961,
    "Cs": 132.90543,
    "Cu": 63.546,
    "Db": 262.1138,
    "Ds": 269.,
    "Dy": 162.5,
    "Er": 167.26,
    "Es": 252.0829,
    "Eu": 151.965,
    "F" : 18.9984032,
    "Fe": 55.845,
    "Fl": 289.,
    "Fm": 257.0951,
    "Fr": 223.0197,
    "Ga": 69.723,
    "Gd":157.25,
    "Ge": 72.61,
    "H" : 1.00794,
    "He": 4.002602,
    "Hf": 178.49,
    "Hg": 200.59,
    "Ho": 164.93032,
    "Hs": 265,
    "I" : 126.90447,
    "In": 114.82,
    "Ir": 192.22,
    "K" : 39.0983,
    "Kr": 83.8,
    "La": 138.9055,
    "Li": 6.941,
    "Lr": 260.1053,
    "Lu": 174.967,
    "Lv": 293.,
    "Mc": 289.,
    "Md": 258.0986,
    "Mg": 24.305,
    "Mn": 54.93805,
    "Mo": 95.94,
    "Mt": 266.,
    "N" : 14.00674,
    "Na": 22.989768,
    "Nb": 92.90638,
    "Nd": 144.24,
    "Ne": 20.1797,
    "Nh": 286.,
    "Ni": 58.69,
    "No": 259.1009,
    "Np": 237.0482,
    "O" : 15.9994,
    "Og": 294.,
    "Os": 190.2,
    "P" : 30.973762,
    "Pa": 231.0359,
    "Pb": 207.2,
    "Pd": 106.42,
    "Pm": 146.9151,
    "Po": 208.9824,
    "Pr": 140.90765,
    "Pt": 195.08,
    "Pu": 244.0642,
    "Ra": 226.0254,
    "Rb": 85.4678,
    "Re": 186.207,
    "Rf": 261.1087,
    "Rg": 272.,
    "Rh": 102.9055,
    "Rn": 222.0176,
    "Ru": 101.07,
    "S" : 32.066,
    "Sb": 121.75,
    "Sc": 44.95591,
    "Se": 78.96,
    "Sg": 263.1182,
    "Si": 28.0855,
    "Sm": 150.36,
    "Sn": 118.71,
    "Sr": 87.62,
    "Ta": 180.9479,
    "Tb": 158.92534,
    "Tc": 98.9063,
    "Te": 127.6,
    "Th": 232.0381,
    "Ti": 47.88,
    "Tl": 204.3833,
    "Tm": 168.93421,
    "Ts": 294.,
    "U" : 238.0289,
    "V" : 50.9415,
    "W" : 183.85,
    "Xe": 131.29,
    "Y" : 88.90585,
    "Yb": 173.04,
    "Zn": 65.39,
    "Zr":91.224,
}

def get_default():
    default_dict={}
    _file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "../..",
            "validation/workflow_parameters.json",
        )
    with open(_file, encoding="utf8") as handler:
        schema = json.load(handler)
        for key in schema["properties"]:
            if "default" in schema["properties"][key]:
                default_dict[key]=schema["properties"][key]["default"]
    return default_dict


def creates_param_dict(yaml_dict,params={}):
    param_dict={}
    default_dict=get_default()
    # initialize with default values
    for key in default_dict:
        param_dict[key]=default_dict[key]
    
    # overwrite with user defined values
    for key in yaml_dict:
        param_dict[key]=yaml_dict[key]
    
    # automatically computes some values
    if 'numodis_dtime' in params and 'numodis_dmax' in params:
        param_dict['timestep']=params['numodis_dtime'] * 1000 / param_dict['lammps_num_step_ratio'] # ps
        param_dict['tsim']=params['numodis_dmax'] * 1000 # ps

    if "ename" in params:
        param_dict["ename"]=params["ename"]

    if param_dict['ename'] in _DEFAULT_EMASS:
        param_dict['emass']=_DEFAULT_EMASS[param_dict['ename']]
    
    if "emass" in params:
        param_dict["emass"]=params["emass"]


    # overwrite again with user defined values
    for key in yaml_dict:
        param_dict[key]=yaml_dict[key]

    return param_dict

def relax_lammps_script(yaml_dict={},extra_dict={},material_ET=''):
    """docstring"""

    params={}
    for child in material_ET:
        if child.tag=="materials":
            for grandchild in child:
                if grandchild.tag=="material":
                    params["ename"]=grandchild.attrib["name"]
                    for grandchild2 in grandchild:
                        if grandchild2.tag=="physics":
                            params["emass"]=grandchild2.attrib["mass"]

    param_dict=creates_param_dict(yaml_dict, params=params)
    
    
    for key in _DEFAULT_EXTRA:
        if key not in extra_dict:
            extra_dict[key]=_DEFAULT_EXTRA[key]

    script=f"""#======================================================================
#             LAMMPS input file to equilibrate a crystal 
#----------------------------------------------------------------------
# Minimize initial configuration, equilibrate at {param_dict['temperature']} K.
#----------------------------------------------------------------------
# Based on the work of Wassim Kassem (18/07/2016)
# and later modified by Laurent Dupuy (CEA).
#----------------------------------------------------------------------
# Last update: May 31th, 2018
#======================================================================

units       {param_dict['units']}           # define units: gr/mole (atomic mass), Angstroms, ps, eV (energy), Kelvin, Angstroms/ps (velocity)...
dimension   3               # 3D
boundary    p      p     f  # layers are stacked along z
atom_style  {param_dict['atom_style']}          # no need for charges or bonds

#------------ Initial unit cell, box and atom positions -----------------------

read_data     atoms.data  # created by numodis in previous step

#------------------ Define regions and groups -------------------------------------
variable      thickness equal {param_dict['thickness']}  # above cut-off radius (7.7 Ansgtroms)
variable      tmp1 equal zhi-${{thickness}}
variable      tmp2 equal zlo+${{thickness}}
region        rg_upper block INF INF INF INF ${{tmp1}} INF       units box 
region        rg_lower block INF INF INF INF INF     ${{tmp2}}   units box
group         upper region rg_upper
group         lower region rg_lower
group         mobileatoms subtract all upper lower
group         boundaryatoms union upper lower

#------------------ mass and interatomic potential --------------------------------
mass          1  {param_dict['emass']}
pair_style    {param_dict['pair_style']}
pair_coeff    * * {param_dict['potential_filename']} {param_dict['ename']}

#============ Minimize forces =====================================================

timestep      {param_dict['min_timestep']}
compute       mype all pe
variable      peatom equal c_mype/count(all)  #PE per atom

thermo        {param_dict['min_thermo']}
thermo_style  custom step v_peatom press lx ly lz

fix           fxFreezmin boundaryatoms setforce 0.0 0.0 0.0

min_style     quickmin
minimize      {param_dict['min_etol']} {param_dict['min_ftol']} {param_dict['min_maxiter']} {param_dict['min_maxeval']}
min_style     cg

unfix         fxFreezmin

#============== Equilibration =====================================================
reset_timestep  0
timestep        {param_dict['eq_timestep']}

variable        T equal {param_dict['temperature']}      # temperature enforce during equilibration [Kelvin]
variable        teq equal {param_dict['teq']}            # equilibration time [ps]

variable        T2	    equal 2*$T
variable        neq     equal ${{teq}}/dt               # number of timesteps

velocity        mobileatoms create ${{T2}} {param_dict['seed']} dist gaussian mom yes
velocity        boundaryatoms set 0 0 0

compute         myEquilibrationTemperature mobileatoms temp

dump            equilibration all custom ${{neq}} dump.equilibration.data id x y z

fix             fxThermostat     mobileatoms nvt temp $T $T 10.0

thermo_style    custom step c_myEquilibrationTemperature v_peatom
thermo          {param_dict['eq_thermo']} # eq_thermo

run             ${{neq}} # {param_dict['teq']} ps

unfix           fxThermostat
undump          equilibration

{extra_dict['restart_line']}
"""
    return script

def load_lammps_script(yaml_dict={},extra_dict={},inputdata_ET='',material_ET=''):

    for key in _DEFAULT_EXTRA:
        if key not in extra_dict:
            extra_dict[key]=_DEFAULT_EXTRA[key]
    params={}

    for child in material_ET:
        if child.tag=="materials":
            for grandchild in child:
                if grandchild.tag=="material":
                    params["ename"]=grandchild.attrib["name"]
                    for grandchild2 in grandchild:
                        if grandchild2.tag=="physics":
                            params["emass"]=grandchild2.attrib["mass"]
    
    for child in inputdata_ET:
        if child.tag=="integration":
            params['numodis_dtime']=float(child.attrib["dtime"]) # ns
            if "nstep" in child.attrib:
                params['numodis_dmax']=params['numodis_dtime']*int(child.attrib["nstep"]) # ns
            elif "dmax" in child.attrib:
                params['numodis_dmax']=float(child.attrib["dmax"]) # ns

        elif child.tag=="loading":
            gammadot_x=0.
            gammadot_y=0.
            gammadot_z=0.
            # strainratedriven
            for grandchild in child:
                if grandchild.attrib["monitor"]=="XZ" or grandchild.attrib["monitor"]=="xz":
                    if "shearrate" in grandchild.attrib :
                        gammadot_x+=float(grandchild.attrib["shearrate"]) / 1000 # ps
                    elif "strainrate" in grandchild.attrib:
                        gammadot_z+=float(grandchild.attrib["strainrate"]) / 1000 # ps
                elif grandchild.attrib["monitor"]=="YZ" or grandchild.attrib["monitor"]=="yz":
                    if "shearrate" in grandchild.attrib :
                        gammadot_y+=float(grandchild.attrib["shearrate"]) / 1000 # ps
                    elif "strainrate" in grandchild.attrib:
                        gammadot_z+=float(grandchild.attrib["strainrate"]) / 1000 # ps
                elif grandchild.attrib["monitor"]=="ZZ" or grandchild.attrib["monitor"]=="zz":
                    if "shearrate" in grandchild.attrib :
                        gammadot_z+=float(grandchild.attrib["shearrate"]) / 1000 # ps
                    elif "strainrate" in grandchild.attrib:
                        gammadot_z+=float(grandchild.attrib["strainrate"]) / 1000 # ps
   
    gammadot_tot=sqrt(gammadot_z**2+gammadot_x**2+gammadot_y**2)

    param_dict=creates_param_dict(yaml_dict,params)

    c1='compute         ff  mobileatoms group/group upper pair yes # calculate force between the upper and mobile atoms'
    c2='compute         ff2 mobileatoms group/group lower pair yes  # calculate force between the lower and mobile atoms'
    c3='compute         ff3 mobileatoms reduce sum fx               # compute total force acting along x on mobile atoms'
    c4='compute         statom mobileatoms stress/atom NULL pair    # stress-per-atom for mobile atoms'
    c5='compute         mypxz  mobileatoms reduce sum c_statom[5]   # total stress for mobile atoms'

    var1='variable        A       equal lx*ly        # area'
    var2='variable        tauxz   equal c_ff[1]/$A   # stress tau_xz'
    var3='variable        tauxz2  equal c_ff2[1]/$A  # stress tau_xz'
    var4='variable        tauxz3  equal c_ff3/$A'


    vart1='variable        tauxzMPa     equal "v_tauxz*1.60218e-19/1e-30*1e-6"  # MPa'
    vart2='variable        tauxzMPav2   equal "v_tauxz2*1.60218e-19/1e-30*1e-6" # MPa'
    vart3='variable        tauxzMPav3   equal "v_tauxz3*1.60218e-19/1e-30*1e-6" # MPa'
    vart4='variable        tauxzMPav4   equal "pxz*0.1"                         # MPa'
    vart5='variable        tauxzMPav5   equal "-c_mypxz/lx/ly/(lz)*0.1"         # MPa'
    
    dicotaux={
        'tauxzMPa':c1+'\n'+var1+'\n'+var2+'\n'+vart1+'\n',
        'tauxzMPav2':c2+'\n'+var1+'\n'+var3+'\n'+vart2+'\n',
        'tauxzMPav3':c3+'\n'+var1+'\n'+var4+'\n'+vart3+'\n',
        'tauxzMPav4':vart4+'\n',
        'tauxzMPav5':c4+'\n'+c5+'\n'+vart5+'\n',
    }

    str_chain=dicotaux[param_dict['tauxzMPa']]
    str_name=param_dict['tauxzMPa']

    script=f"""#======================================================================
#             LAMMPS input file to shear a crystal 
#----------------------------------------------------------------------
# Minimize initial configuration, equilibrate at {param_dict['temperature']} K.
# apply shear at a given rate in the z plane along x direction
#----------------------------------------------------------------------
# Based on the work of Wassim Kassem (18/07/2016)
# and later modified by Laurent Dupuy (CEA).
#----------------------------------------------------------------------
# Last update: June 1st, 2018
#-----------------------------------------------------------------------
#======================================================================

units       {param_dict['units']}           # define units: gr/mole (atomic mass), Angstroms, ps, eV (energy), Kelvin, Angstroms/ps (velocity)...
dimension   3               # 3D
boundary    p      p     f  # layers are stacked along z
atom_style  {param_dict['atom_style']}          # no need for charges or bonds

#------------ Read atom positions and velocities -----------------------

read_restart  lammps.restart

#------------------ Define regions and groups -------------------------------------
variable      thickness equal {param_dict['thickness']}  # above cut-off radius (7.7 Ansgtroms)
variable      tmp1 equal zhi-${{thickness}}
variable      tmp2 equal zlo+${{thickness}}
region        rg_upper block INF INF INF INF ${{tmp1}} INF       units box 
region        rg_lower block INF INF INF INF INF     ${{tmp2}}   units box
group         upper region rg_upper
group         lower region rg_lower
group         mobileatoms subtract all upper lower
group         boundaryatoms union upper lower

#------------------ mass and interatomic potential --------------------------------
mass        1  {param_dict['emass']}
pair_style  {param_dict['pair_style']}
pair_coeff  * * {param_dict['potential_filename']} {param_dict['ename']}

################################################################################################

label           deform

reset_timestep  0
timestep        {param_dict['timestep']}

variable        tsim        equal {param_dict['tsim']}           # total simulation time [ps] - default  value
variable        tprob       equal {param_dict['tprob']}          # time to dump stress for SIGEPS.dat [ps] - default value
variable        tthermo     equal {param_dict['tthermo']}        # time to dump thermodynamic properties [ps] - default value
variable        tdump       equal {param_dict['tdump']}          # time to dump an output file -- cannot be smaller than tprob for movie creation [ps] - default value
variable        save_restart   equal {param_dict['save_restart']} # write a restart file this every steps - default value


variable        nsteps      equal ${{tsim}}/dt
variable        n2steps     equal ${{nsteps}}*2
variable        ndump       equal ${{tdump}}/dt
variable        nprob       equal ${{tprob}}/dt #2000 points per graph
variable        nthermo     equal ${{tthermo}}/dt
variable        nrestart    equal stride(0,${{n2steps}},${{save_restart}})

velocity        boundaryatoms set 0 0 0

variable        vx0         equal (lz-2*${{thickness}})*{gammadot_x}
variable        vy0         equal (lz-2*${{thickness}})*{gammadot_y}
variable        vz0         equal (lz-2*${{thickness}})*{gammadot_z}

variable        gammadot    equal {gammadot_tot}

compute         myTemperature mobileatoms temp

{str_chain}

thermo_style    custom step temp press
run             0

variable        time         equal step*dt                    # time [ps]
variable        gamma        equal ${{gammadot}}*v_time*100   # shear strain [%]	

thermo          ${{nthermo}}
thermo_style    custom &
        v_time v_gamma v_{str_name} press c_myTemperature

fix             fxAverage all ave/time 1 ${{nprob}} ${{nprob}} &
        v_time v_gamma v_{str_name} &
                off 1 off 2 file SIGEPS.dat &
                title2 "#Step time(ps) gamma(%) Fx_upper/lx/ly(MPa) Fx_lower/lx/ly(MPa) Pxz(MPa) Pxz_mobile(MPa)"

fix             fxInstantaneous all  print  ${{nprob}} &
        "${{time}} ${{gamma}} ${{{str_name}}}" &
        append SIGEPS.instantaneous.dat screen no

dump            deform all custom ${{ndump}} deformed.data id x y z

fix             fxShear upper move variable NULL NULL NULL v_vx0 v_vy0 v_vz0

fix             fxRunNVE mobileatoms nve

run             ${{nsteps}} # variable 
"""
    return script