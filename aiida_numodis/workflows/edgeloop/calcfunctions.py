import io
import logging
import tempfile
from aiida import orm
from aiida.engine import calcfunction
from aiida.orm import SinglefileData, Dict
from aiida.common import AttributeDict
from aiida_numodis.data import NumodisXmlData
import yaml
from .utils import relax_lammps_script, load_lammps_script
from pathlib import Path


@calcfunction
def edgeloop_setup( node_datafile0, node_datafile2,node_datafile5, node_potential):
    logger=logging.getLogger(__name__)
    potential_name=Path(node_potential.value).name

    result={
        'dt_node0':NumodisXmlData(node_datafile0.value),
        'dt_node2':NumodisXmlData(node_datafile2.value),
        'dt_node5':NumodisXmlData(node_datafile5.value),
        'lpt_snode':SinglefileData(file=node_potential.value,filename=potential_name),
    }
    return result

@calcfunction
def prepareMD1( node_workflow_parameters, node_DD0structures, potential_node, node_DD0_xml):
    logger=logging.getLogger(__name__)

    with node_workflow_parameters.open() as stream :
        yaml_dict=yaml.safe_load(stream)
    yaml_dict['potential_filename']=potential_node.filename

    node_DD0_xml.read_data()
    str_script=relax_lammps_script(yaml_dict, material_ET=node_DD0_xml.numodis_parameters.materials[0])

    byte_object=io.BytesIO(str_script.encode())
    SCRIPT=SinglefileData(file=byte_object, filename='in.data.equilibrate')

    OPTIONS = AttributeDict()
    OPTIONS.resources = AttributeDict()
    OPTIONS.resources.num_machines = 1
    OPTIONS.resources.tot_num_mpiprocs = 2
    OPTIONS.withmpi=True
    OPTIONS.trajectory_filename="dump.equilibration.data"
    settings_dict={'additional_retrieve_list':['dump.equilibration.data']}
    SETTINGS = orm.Dict(settings_dict)

    _metadata=AttributeDict()
    _metadata.options=OPTIONS
    metadata=Dict(_metadata)

    structure_file=SinglefileData(file=node_DD0structures.get_file('atoms.data'), filename='atoms.data')

    result={
        'script': SCRIPT, 
        'metadata':metadata,
        'settings':SETTINGS,
        'structure':structure_file}

    return result

@calcfunction
def prepareDD2(lammps_data_node):

    # Load Numodis input files
    # Retrieve LAMMPS dump file
    trajectory_handle=tempfile.NamedTemporaryFile()
    lammps_data_node.write_as_lammps(trajectory_handle)

    # Flush and rewind the temporary handle,
    # otherwise the command to store it in the repo will write an empty file
    trajectory_handle.flush()
    trajectory_handle.seek(0)

    # Copy LAMMPS dump file to Numodis inputs
    lammps_file= SinglefileData(file=trajectory_handle,filename="dump.equilibration.data")

    results={
        "lammpsdata":lammps_file,
    }
    return results

@calcfunction
def prepareMD3( node_workflow_parameters, node_DD2structures, potential_node,node_DD2_xml): 
    logger=logging.getLogger(__name__)
    with node_workflow_parameters.open() as stream :
        yaml_dict=yaml.safe_load(stream)
    yaml_dict['potential_filename']=potential_node.filename

    OPTIONS = AttributeDict()
    OPTIONS.resources = AttributeDict()
    OPTIONS.resources.num_machines = 1
    OPTIONS.resources.tot_num_mpiprocs = 2
    
    OPTIONS.withmpi=True
    OPTIONS.trajectory_filename="dump.equilibration.data"
    OPTIONS.restart_filename='equilibration.restart'
    settings_dict={'additional_retrieve_list':['dump.equilibration.data','equilibration.restart']}
    SETTINGS = orm.Dict(settings_dict)
    _metadata=AttributeDict()
    _metadata.options=OPTIONS

    extra_dict={
        'restart_line':'write_restart equilibration.restart'
        }
    node_DD2_xml.read_data()
    str_script=relax_lammps_script(yaml_dict, 
                                   extra_dict,
                                   material_ET=node_DD2_xml.numodis_parameters.materials[0])

    byte_object=io.BytesIO(str_script.encode())
    SCRIPT=SinglefileData(file=byte_object, filename='in.data.equilibrate')

    lammps_input=SinglefileData(file=node_DD2structures.get_file('atoms.data'), filename='atoms.data')
    metadata=Dict(_metadata)
    result={
        'script':SCRIPT,
        'structure': lammps_input,
        'settings': SETTINGS,
        'metadata':metadata}

    return result

@calcfunction
def prepareMD4( node_workflow_parameters, node_DD5_xml,potential_node):
    logger=logging.getLogger(__name__)
    
    with node_workflow_parameters.open() as stream :
        yaml_dict=yaml.safe_load(stream)
    yaml_dict['potential_filename']=potential_node.filename
    OPTIONS = AttributeDict()
    OPTIONS.resources = AttributeDict()
    OPTIONS.resources.num_machines = 1
    OPTIONS.resources.tot_num_mpiprocs = 2
    OPTIONS.withmpi=True
    OPTIONS.extra_filenames=["SIGEPS.dat",'SIGEPS.instantaneous.dat']
    OPTIONS.trajectory_filename="deformed.data"
    OPTIONS.restart_filename='lammps.restart'
    _metadata=AttributeDict()
    _metadata.options=OPTIONS
    metadata=Dict(_metadata)
    
    node_DD5_xml.read_data()
    str_script=load_lammps_script(
        yaml_dict,
        inputdata_ET=node_DD5_xml.numodis_parameters.inputdata,
        material_ET=node_DD5_xml.numodis_parameters.materials[0]
        )
    
    byte_object=io.BytesIO(str_script.encode())
    SCRIPT=SinglefileData(file=byte_object, filename='in.data.shear')

    settings_dict={'additional_retrieve_list':['deformed.data', 'SIGEPS.instantaneous.dat', 'lammps.restart', 'SIGEPS.dat']}
    SETTINGS = orm.Dict(settings_dict)

    result={ 
        'script':SCRIPT,
        'settings':SETTINGS,
        'metadata':metadata}

    return result