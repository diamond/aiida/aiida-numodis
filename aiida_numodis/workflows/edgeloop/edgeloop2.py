"""
Workflow using numodis and LAMMPS.
Described in the section 2.1.3 of L M Dupuy et al 2024 Modelling Simul. Mater. Sci. Eng. 32 035015, (https://doi.org/10.1088/1361-651X/ad278a)
Benchmark 141 of numodis

requires aiida-lammps plugin (Diamond version) available at https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/aiida/aiida-lammps

TODO: read LAMMPS parameters from NUMODIS
TODO: better documentation

change edge to DD/MD
add further steps

# Tutorial
## How to use the edgeloop2 workflow

python launch_edgeloop2.py
verdi process list -a
verdi node show <PK>
verdi node graph generate <PK>
verdi data core.singlefile content <PK> 
"""

import logging
from aiida import orm
from aiida.engine import ToContext, WorkChain
from aiida.orm import AbstractCode, Str, SinglefileData, Dict, load_node
from aiida.plugins.factories import CalculationFactory
from aiida.common import AttributeDict
from .calcfunctions import edgeloop_setup, prepareMD1, prepareDD2, prepareMD3, prepareMD4
from aiida_numodis.validation.utils import validate_against_schema
import yaml

numodisRaw = CalculationFactory('numodis.raw')
lammpsBase = CalculationFactory('lammps.base')
lammpsRaw = CalculationFactory('lammps.raw')

def edgeloop_deco(step_id: str, step_code: str,step_default: str):
    """
    wrap decorator to define decorator parameters.
    """
    def edgeloop_step(step_function):
        """Decorator for each step of edgeloop workflow"""
        def inner_wrapper(self,*parameters, **kwargs):
            """
            """

            logger=logging.getLogger(__name__)
            logger.info("start "+step_id+" step")
            
            # check if the node should be replaced by a previous, already computed, node
            if 'replacement_nodes' in self.inputs:
                dico=self.inputs.replacement_nodes.get_dict()
            else:
                dico={}

            if step_id in dico:
                # load previous node from pk number
                previous_node=load_node(pk=dico[step_id])
                input={step_id:previous_node}
                return ToContext(**input)
            else:
                # check if a specific code is given for this node and use the default one otherwise
                if step_code in self.inputs:
                    code=self.inputs[step_code]
                else:
                    code=self.inputs[step_default]
                
                output_node=step_function(self, code, *parameters, **kwargs)

                # node info to be displayed with verdi node show #
                output_node.label=step_function.__name__
                output_node.description=step_function.__doc__
                input={step_id:output_node}
                return ToContext(**input)

        return inner_wrapper
    return edgeloop_step


class edgeloop2WorkChain(WorkChain):
    """Workflow for atomistically informed dislocation dynamics simulations.                                                       
    This workflow runs 6 steps calling NUMODIS and LAMMPS:                                         
                                                                                                   |
     ______________________                               ________________                         |
    | Dislocation creation |      atoms positions        |   Relaxation   |                        |
    |   (NUMODIS - DD0)    | ==========================> | (LAMMPS - MD1) |                        |
    |______________________|                             |________________|                        |
                                                                  |                                |
                 _________________________________________________/                                |
                /                 atoms positions                                                  |
     __________v___________                               ________________                         |
    | Introducing defaults |                             |   Relaxation   |                        |
    |   (NUMODIS - DD2)    | ==========================> | (LAMMPS - MD3) |                        |
    |______________________|      atoms positions        |________________|                        |
                                                                 |                                 |
     ______________________                              ________v__________                       |
    |  Mechanical load     |                            |  Mechanical load  |                      |
    |   (NUMODIS - DD5)    | <   synchronized inputs  > | (LAMMPS - MD4)    |                      |
    |______________________|                            |___________________|                      |
                                                                                                   |
                                                                                                   
    Results of the steps DD5 and MD4 can be used to compare both methods.
See more information in : L M Dupuy et al 2024 Modelling Simul. Mater. Sci. Eng. 32 035015, (https://doi.org/10.1088/1361-651X/ad278a)
    """ # displayed with verdi

    @classmethod
    def define(cls, spec):
        """Specify inputs and outputs"""
        super().define(spec)
        spec.input('data_file0', valid_type=Str, help='Absolute path to data_files.xml Numodis input file for DD.0 step.')
        spec.input('data_file2', valid_type=Str, help='Absolute path to data_files.xml Numodis input file for DD.2 step.')
        spec.input('data_file5', valid_type=Str, help='Absolute path to data_files.xml Numodis input file for DD.5 step.')
        spec.input('code_numodis', valid_type=AbstractCode, help='Default Numodis code, to use in all Dislocation Dynamics (DD) steps.')
        spec.input('code_lammps', valid_type=AbstractCode, help='Default LAMMPS code, to use in all Molecular Dynamics (MD) steps.')
        
        spec.input('code_numodis0', valid_type=AbstractCode, required=False, help='Numodis code to use for the step 0 (DD0) instead of default.')
        spec.input('code_lammps1', valid_type=AbstractCode, required=False, help='LAMMPS code, to use for the step 1 (MD1) instead of default.')
        spec.input('code_numodis2', valid_type=AbstractCode, required=False, help='Numodis code to use for the step 2 (DD2) instead of default.')
        spec.input('code_lammps3', valid_type=AbstractCode, required=False, help='LAMMPS code, to use for the step 3 (MD3) instead of default.')
        spec.input('code_lammps4', valid_type=AbstractCode, required=False, help='LAMMPS code, to use for the step 4 (MD4) instead of default.')
        spec.input('code_numodis5', valid_type=AbstractCode, required=False, help='Numodis code to use for the step 5 (DD5) instead of default.')

        spec.input('replacement_nodes', valid_type=Dict, required=False, help='Dictionnary used to restart workflow from previous computations. Assign a node PK to each step (DD0, MD1, DD2, MD3, MD4, DD5) that should be replaced.')
        
        spec.input('lammps_potential', valid_type=Str, help='Absolute path to LAMMPS potential file used for all Molecular Dynamics (MD) steps.')
        spec.input('workflow_parameters', valid_type=SinglefileData, required=False, help='YAML file with parameters for this workflow.')
        spec.inputs.validator = cls._validate_inputs

        spec.outline(
            cls.setup,
            cls.DislocationDynamics0,
            cls.MolecularDynamics1,
            cls.DislocationDynamics2,
            cls.MolecularDynamics3,
            cls.MolecularDynamics4,
            cls.DislocationDynamics5,
            cls.result,
        )

        spec.inputs['metadata']["label"].default="edgeloop2 workflow"
        spec.inputs['metadata']["description"].default="Workflow for atomistically informed dislocation dynamics simulations."

        spec.output('result', valid_type=orm.Dict)
    
    @classmethod
    def _validate_inputs(cls, value, ctx):
        """
        Validate workflow_parameters.yml
        """

        if "workflow_parameters" in value:
            
            with value["workflow_parameters"].open() as stream :
                yaml_dict=yaml.safe_load(stream)
            return validate_against_schema(yaml_dict)
        else:
            return "No workflow parameters"

    def setup(self):
        """
        """
        # sequential workflow

        logger=logging.getLogger(__name__)

        setup_nodes_dict = edgeloop_setup(
            node_datafile0 = self.inputs.data_file0,
            node_datafile2 = self.inputs.data_file2,
            node_datafile5 = self.inputs.data_file5,
            node_potential = self.inputs.lammps_potential,
            )

        self.ctx.inputs=AttributeDict()
        for key in setup_nodes_dict:
            if key[:3]!='tmp':
                self.ctx.inputs[key]=setup_nodes_dict[key]

        if "tmp_lpt_node" in setup_nodes_dict:
            self.ctx.inputs["lpt_node"]=load_node(uuid=setup_nodes_dict["tmp_lpt_node"].value)

    @edgeloop_deco("DD0", "code_numodis0", "code_numodis")
    def DislocationDynamics0(self, code_numodis):
        """Dislocation creation with NUMODIS.
        Creation of two perfect half-crystals.
        """
        inputs = {'data_files':self.ctx.inputs.dt_node0, 'code':code_numodis}

        return self.submit(numodisRaw, **inputs)
        
    @edgeloop_deco("MD1", "code_lammps1","code_lammps")
    def MolecularDynamics1(self, code_lammps):
        """First relaxation step with LAMMPS."""
        results=prepareMD1(
            node_workflow_parameters=self.inputs.workflow_parameters,
            node_DD0structures=self.ctx.DD0.outputs.structures,
            potential_node=self.ctx.inputs.lpt_snode,
            node_DD0_xml=self.ctx.inputs.dt_node0)

        files_dict={
            'structure':results['structure'],
            'potential':self.ctx.inputs.lpt_snode
            }
        filenames_dict={
            'structure':'atoms.data',
            'potential':self.ctx.inputs.lpt_snode.filename
        }

        inputs={ 
            'code':code_lammps,
            'metadata':results['metadata'].get_dict(),
            'script':results['script'],
            'files':files_dict,
            'filenames':filenames_dict,
            'settings':results['settings'],
            }
        return self.submit(lammpsRaw, **inputs)

    @edgeloop_deco("DD2", "code_numodis2","code_numodis")
    def DislocationDynamics2(self, code_numodis):
        """Introducing dislocations with NUMODIS."""
        results=prepareDD2(self.ctx.MD1.outputs.trajectories)
        inputs = {'data_files':self.ctx.inputs.dt_node2,'lammps_data':results["lammpsdata"], 'code':code_numodis}
        return self.submit(numodisRaw, **inputs)
    
    @edgeloop_deco("MD3", "code_lammps3","code_lammps")
    def MolecularDynamics3(self, code_lammps):
        """Second relaxation step with LAMMPS."""
        results=prepareMD3(
            node_workflow_parameters = self.inputs.workflow_parameters,
            node_DD2structures = self.ctx.DD2.outputs.structures,
            potential_node=self.ctx.inputs.lpt_snode,
            node_DD2_xml=self.ctx.inputs.dt_node2)

        files_dict={
            'structure':results['structure'],
            'potential':self.ctx.inputs.lpt_snode
            }
        filenames_dict={
            'structure':'atoms.data',
            'potential':self.ctx.inputs.lpt_snode.filename
        }

        inputs={ 
            'code':code_lammps, 
            'settings':results['settings'],
            'metadata':results['metadata'].get_dict(),
            'script':results['script'],
            'files':files_dict,
            'filenames':filenames_dict,
            }

        return self.submit(lammpsRaw, **inputs)

    @edgeloop_deco("MD4", "code_lammps4","code_lammps")
    def MolecularDynamics4(self, code_lammps):
        """Load simulation with LAMMPS."""
        results=prepareMD4(
            node_workflow_parameters=self.inputs.workflow_parameters,
            node_DD5_xml=self.ctx.inputs.dt_node5,
            potential_node=self.ctx.inputs.lpt_snode)

        files_dict={
            'restart':self.ctx.MD3.outputs.restartfile,
            'potential':self.ctx.inputs.lpt_snode
            }
        filenames_dict={
            'restart':'lammps.restart',
            'potential':self.ctx.inputs.lpt_snode.filename
        }

        inputs={ 
            'code':code_lammps,
            'metadata':results['metadata'].get_dict(),
            'settings':results['settings'],
            'script':results['script'],
            'files':files_dict,
            'filenames':filenames_dict,
            }

        return self.submit(lammpsRaw, **inputs)
    
    @edgeloop_deco("DD5", "code_numodis5","code_numodis")
    def DislocationDynamics5(self, code_numodis):
        """Load simulation with NUMODIS."""

        inputs = {'data_files':self.ctx.inputs.dt_node5, 'code':code_numodis}
        return self.submit(numodisRaw, **inputs)
    	
    def result(self):
        """Output results."""
        logger=logging.getLogger(__name__)
        logger.debug(self.ctx.DD5.outputs.log.get_content())
        logger.info("End of the workflow")
        self.out('result', self.ctx.MD4.outputs.results)