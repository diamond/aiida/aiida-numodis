#! /usr/bin/env python
"""
Utilitaries for aiida_numodis plugin. Mostly logging.

To configure aiida_numodis logs use :
``aiida_numodis.utils.init_logs()``

To disable non-critical logs use :
``aiida_numodis.utils.init_logs(level='CRITICAL')``
"""
import logging

class CustomFormatter(logging.Formatter):
    """
    Log formatter, adapted from https://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output.
    Requires ANSI escape codes, probably doesn't work on windows.
    """
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    green = "\x1b[32;20m"
    blue = "\x1b[34;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(asctime)s - %(levelname)s - %(filename)s:%(lineno)d - %(message)s"

    FORMATS = {
        logging.DEBUG: blue + format + reset,
        logging.INFO: green + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

def init_logs(module='aiida_numodis',level='WARNING'):
    """
    Initialize logs.
    
    :param module: module to apply log configuration to.
    :type module: str
    :param level: level of logging ('DEBUG','INFO','WARNING','ERROR','CRITICAL')
    :type level: str
    """
    logger = logging.getLogger(module)
    logger.propagate = False
    if level=='DEBUG':
        logger.setLevel(logging.DEBUG)
    elif level=='INFO':
        logger.setLevel(logging.INFO)
    elif level=='WARNING':
        logger.setLevel(logging.WARNING)
    elif level=='ERROR':
        logger.setLevel(logging.ERROR)
    elif level=='CRITICAL':
        logger.setLevel(logging.CRITICAL)
    else:
        logger.setLevel(logging.WARNING)
    
    ch = logging.StreamHandler()
    ch.setFormatter(CustomFormatter())
    logger.addHandler(ch)
    logging.basicConfig()
