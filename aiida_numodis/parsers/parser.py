#! usr/bin/env python

from aiida.engine import ExitCode
from aiida.orm import SinglefileData
from aiida.parsers.parser import Parser
from aiida.plugins import CalculationFactory
from aiida_numodis.data.graph_output import LammpsStructureFiles
from aiida_numodis.data.vtk_output import NumodisVtkFiles
import logging
import pathlib

#NumodisCalculation = CalculationFactory('numodis.raw')

class NumodisParser(Parser):

    def output_filename(self):
        return self.node.get_option('output_filename')

    def parse(self, **kwargs):
        """
        Parse outputs, store results in database.
        """
        logger=logging.getLogger(__name__)
        output_filename = self.output_filename()
        
        # check outputs
        retrieved_files=[]
        for file_obj in self.retrieved.list_objects():
            filename=file_obj.name
            logger.debug('found file : '+filename)
            retrieved_files.append(filename)

        # Parse log file
        with self.retrieved.open(output_filename, 'r') as handle:
            output_node = SinglefileData(file=handle, filename=output_filename)
        self.out('log', output_node)
        
        data_node=self.node.inputs.data_files
        # read xml input files to get data
        data_node.read_data()
        
        # Parse structure graphs
        structure_files_list=data_node.numodis_parameters.output_path.graphs
        output_structures=LammpsStructureFiles()
        for filepath in structure_files_list:
            filename=pathlib.Path(filepath).name
            logger.debug("Parsing structure file : "+filename)
            with self.retrieved.open(filename, 'r') as handle:
                output_structures.add_struct_file(handle, key=filename)
        self.out('structures', output_structures)
        
        # Parse vtk
        vtk_files_list=data_node.numodis_parameters.output_path.defects
        output_vtk=NumodisVtkFiles()
        for filepath in vtk_files_list:
            filename=pathlib.Path(filepath).name
            logger.debug("Parsing vtk file : "+filename)
            with self.retrieved.open(filename, 'r') as handle:
                output_vtk.add_vtk_file(handle, key=filename)
        self.out('vtk_files', output_vtk)

        return ExitCode(0)