import tkinter as tk
from tkinter import ttk
import yaml
import sys

import os
import json

def load_button_clicked(enter_yml, root):
    filename=enter_yml.get()
    _file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "..",
        "validation/workflow_parameters.json",
        )
    with open(_file, encoding="utf8") as handler:
        schema = json.load(handler)

    with open(filename,'r') as stream :
        yaml_dict=yaml.safe_load(stream)

    root.destroy()
    Home(filename, yaml_dict, schema)

def save_button_clicked(enter_yml, entry_list,schema):
    filename=enter_yml.get()
    with open(filename,'w') as handle:
        handle.write("---\n")
        for entry in entry_list:
            key=entry[0]
            value=entry[1].get()
            if key in schema["properties"]:
                if "type" in schema["properties"][key]:
                    if schema["properties"][key]["type"]=="number":
                        value=value.replace('e','.e')
            handle.write(key+": "+value+"\n")

def create_add_param(key,window,filename,yaml_dict,schema):
    def add_param():
        if "default" in schema["properties"][key]:
            yaml_dict[key]=schema["properties"][key]["default"]
        else:
            yaml_dict[key]=""
        window.destroy()
        Home(filename, yaml_dict, schema)
    return add_param

def return_home(window,filename,yaml_dict,schema):
    window.destroy()
    Home(filename, yaml_dict, schema)

def create_delete_button_clicked(key, root,schema,enter_yml,entry_list):
    def delete_button_clicked():
        yaml_dict={}
        filename=enter_yml.get()
        for entry in entry_list:
            if entry[0]!=key:
                yaml_dict[entry[0]]=entry[1].get()
        root.destroy()
        Home(filename, yaml_dict, schema)
    return delete_button_clicked

def add_parameter(root,enter_yml,entry_list,schema):
    filename=enter_yml.get()
    yaml_dict={}
    for entry in entry_list:
        yaml_dict[entry[0]]=entry[1].get()
    root.destroy()
    add_parameter_window(filename, yaml_dict, schema)

class CreateToolTip(object):
    """
    create a tooltip for a given widget
    """
    def __init__(self, widget, text='widget info'):
        self.waittime = 0     #miliseconds
        self.wraplength = 180   #pixels
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.leave)
        self.widget.bind("<ButtonPress>", self.leave)
        self.id = None
        self.tw = None

    def enter(self, event=None):
        self.schedule()

    def leave(self, event=None):
        self.unschedule()
        self.hidetip()

    def schedule(self):
        self.unschedule()
        self.id = self.widget.after(self.waittime, self.showtip)

    def unschedule(self):
        id = self.id
        self.id = None
        if id:
            self.widget.after_cancel(id)

    def showtip(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(self.tw, text=self.text, justify='left',
                       background="#ffffff", relief='solid', borderwidth=1,
                       wraplength = self.wraplength)
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tw
        self.tw= None
        if tw:
            tw.destroy()


def Home(filename, yaml_dict, schema):
    # create window
    root = tk.Tk()
    
    # add window title
    root.title(schema["title"])

    # if no yaml_dict create empty dictionnary
    if yaml_dict==None:
        yaml_dict={}
    
    # add default required values to yaml_dict
    for key in schema["properties"]:
        if key in schema["required"] and not key in yaml_dict:
            if "default" in schema["properties"][key]:
                yaml_dict[key]=schema["properties"][key]["default"]
            else:
                yaml_dict[key]=""

    # number of keys
    n_keys=len(yaml_dict)

    # Creating the window grid
    for i in range(n_keys+4):
        root.grid_rowconfigure(index=i, weight=1, uniform="same_group")
    for i in range(5):
        root.grid_columnconfigure(index=i, weight=1, uniform="same_other_group")
    
    # Header
    message = tk.Label(root, text=schema["title"])
    message.grid(row=0, column=0, sticky='E', padx=20)

    # filename label
    lbl = ttk.Label(root,text="filename :", anchor="e")
    info_msg="yaml file name to be loaded or saved."
    info_ttp=CreateToolTip(lbl, info_msg)
    lbl.grid(row=n_keys+2, column=0, sticky='E', padx=20)

    # filename entry box
    enter_yml = ttk.Entry(root)
    enter_yml.insert(0,filename)
    enter_yml.grid(row=n_keys+2, column=1, columnspan=2, sticky='WE', padx=20)
    
    # Parameters
    entry_list=[]
    i=0
    for key in yaml_dict:
        # parameter label
        lbl = ttk.Label(root,text=key+":", anchor="e")
        lbl.grid(row=1+i, column=2, sticky='E', padx=20)

        # parameter entry box
        enter = ttk.Entry(root)
        enter.insert(0,yaml_dict[key])
        enter.grid(row=1+i, column=3, columnspan=2, sticky='WE', padx=20)
        entry_list.append((key,enter))

        # if not required, add a delete button
        if key not in schema["required"]:
                delete_func=create_delete_button_clicked(key, root,schema,enter_yml, entry_list)
                delete_button = ttk.Button(root, text="delete", command=delete_func)
                delete_button.grid(row=1+i, column=1, sticky='E', padx=20)

        # add description tooltip
        if key in schema["properties"]:
            info_msg=""
            if "description" in schema["properties"][key]:
                info_msg=schema["properties"][key]["description"]
            if "default" in schema["properties"][key]:
                info_msg+="\nExample: "+str(schema["properties"][key]["default"])
            if info_msg=="" :
                info_msg="No description."
        else:
            info_msg="Unknown parameter"
        
        info_ttp=CreateToolTip(lbl, info_msg)
                    
        i+=1
    
    # add_button
    add_button = ttk.Button(root, text="add parameter", command=lambda: add_parameter(root, enter_yml, entry_list, schema))
    add_button.grid(row=n_keys+1, column=0, sticky='E', padx=20)

    # load_button
    load_button = ttk.Button(root, text="load", command=lambda: load_button_clicked(enter_yml,root))
    load_button.grid(row=n_keys+2, column=3, sticky='E', padx=20)
    # save_button
    save_button = ttk.Button(root, text="save", command=lambda: save_button_clicked(enter_yml, entry_list, schema))
    save_button.grid(row=n_keys+2, column=4, sticky='E', padx=20)

    root.mainloop()


def add_parameter_window(filename, yaml_dict, schema):
    window=tk.Tk()
    
    # add window title
    window.title("add parameters")

    nb_parameters=0
    for key in schema["properties"]:
        if not key in yaml_dict:
            nb_parameters+=1
    
    # Creating the window grid
    for i in range(nb_parameters+2):
        window.grid_rowconfigure(index=i, weight=1, uniform="same_group")
    for i in range(10):
        window.grid_columnconfigure(index=i, weight=1, uniform="same_other_group")
    

    # parameter header
    lbl = ttk.Label(window,text="Parameter", anchor="w")
    lbl.grid(row=1, column=1, sticky='W', padx=20)

    # type header
    lbl = ttk.Label(window,text="Type", anchor="e")
    lbl.grid(row=1, column=2, sticky='E', padx=20)

    # default header
    lbl = ttk.Label(window,text="Default value", anchor="w")
    lbl.grid(row=1, column=3, sticky='W', padx=20)

    # description
    lbl = ttk.Label(window,text="Description", anchor="w")
    lbl.grid(row=1, column=4, sticky='W', padx=20)

    i=1
    for key in schema["properties"]:
        if not key in yaml_dict:
            # add button
            add_button = ttk.Button(window, text="+", command=create_add_param(key,window,filename,yaml_dict,schema))
            add_button.grid(row=1+i, column=0, sticky='E', padx=20)
            
            # parameter label
            lbl = ttk.Label(window,text=key, anchor="w")
            lbl.grid(row=1+i, column=1, sticky='W', padx=20)

            # type
            if "type" in schema["properties"][key]:
                lbl = ttk.Label(window,text=schema["properties"][key]["type"], anchor="e")
                lbl.grid(row=1+i, column=2, sticky='E', padx=20)
            
            # Default
            if "default" in schema["properties"][key]:
                lbl = ttk.Label(window,text=schema["properties"][key]["default"], anchor="w")
                lbl.grid(row=1+i, column=3, sticky='W', padx=20)

            # Description
            if "description" in schema["properties"][key]:
                lbl = ttk.Label(window,text=schema["properties"][key]["description"], anchor="w")
                lbl.grid(row=1+i, column=4,columnspan=6, sticky='W', padx=20)
            i+=1

    # return button
    return_button = ttk.Button(window, text="return", command=lambda: return_home(window,filename,yaml_dict,schema))
    return_button.grid(row=nb_parameters+2, column=4, sticky='E', padx=20)

    window.mainloop()

_file = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "..",
    "validation/workflow_parameters.json",
    )
with open(_file, encoding="utf8") as handler:
    schema = json.load(handler)

if len(sys.argv)>1:
    filename=sys.argv[1]
else:
    filename="workflow_parameters.yml"

import pathlib
filename=pathlib.Path(os.getcwd(),filename)
try:
    with open(filename,'r') as stream :
        yaml_dict=yaml.safe_load(stream)
except:
    print(str(filename)+" file doesn't exist")
    yaml_dict={}

Home(filename, yaml_dict, schema)