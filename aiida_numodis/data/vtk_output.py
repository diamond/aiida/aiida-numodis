"""
Module for storing NUMODIS output data
"""

from aiida.orm import Data
import logging

class NumodisVtkFiles(Data):
    """
    """

    def __init__(self):
        """
        """
        
        logger=logging.getLogger(__name__)  # log
        super().__init__()


    def add_vtk_file(self,handle, key='defects.vtk'):
        """
        compress vtk ?
        :param handle: structure file, typically defects.vtk
        :type handle: File like object
        """
        logger=logging.getLogger(__name__)  # log

        # store vtk file
        self.base.repository.put_object_from_filelike(handle,key)

    def get_file(self,key="defects.vtk"):
        """
        """
        # get stored vtk file
        with self.base.repository.open(key,'rb') as vtkfile:
            return vtkfile.read()