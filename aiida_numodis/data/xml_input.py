"""
Module for the data structure containing Numodis .xml input files.

TODO: validation, check .xml input files are valid.
TODO: clean up this mess.
"""

import pathlib
from aiida.orm import FolderData
import logging
import xml.etree.ElementTree as ET
from aiida.common import AttributeDict
from aiida_numodis.data.path_utils import get_source_and_target



class NumodisXmlData(FolderData):
    """
    FolderData to store all Numodis .xml input files.
    Reads Numodis data_files.xml input file to search for all .xml input files to include.
    Stores location of the original data_files.xml file copy in the target folder and the location of the save directory where Numodis will write its results.
    """
    
    def __init__(self,file_path=None):
        logger=logging.getLogger(__name__)  # log
        super().__init__()
        if file_path==None:
            self.make_data()
        else:
            self.store_datafiles(file_path)
                
    def make_data(self):
        """Creates standard numodis input files"""
        #TODO
        return 0
    
    def store_datafiles(self,file_path):
        """read data_files.xml input file, store all related .xml input files"""
        
        # Read data_files.xml file
        source_list,target_list=get_source_and_target(file_path)
        
        # store input files into folder
        for i,filepath in enumerate(source_list):
            self.put_object_from_file(filepath,target_list[i])

        # store entry file relative path
        self.base.attributes.set("data_file_path", str(target_list[0]))
        
    def read_data(self):
        """Get Numodis parameters from xml input files.
        This data is not stored."""
        
        # initialize numodis parameters
        self.numodis_parameters=AttributeDict()

        # Read datafile.xml and store relative paths to other input files
        self.numodis_parameters['relative_path']=AttributeDict(self.read_data_file())

        # read graphs xml files.
        self.get_graph()

        # read inputdata xml file.
        self.get_inputdata()

        # read materials xml files.
        self.get_materials()

        # list output files
        self.get_outputs()

    def read_data_file(self):
        """read data_file.xml to get relative path of other input files."""
        
        xmlstring=self.get_object_content(self.base.attributes.get("data_file_path"))
        path_from_top=pathlib.Path(self.base.attributes.get("data_file_path")).parent
        files_dict={}
        root=ET.fromstring(xmlstring)
        for child in root:
            if child.attrib:
                files_dict[child.tag]=str(pathlib.Path(path_from_top,child.attrib['path_name']))
            else:
                files_dict[child.tag]=[]
                for grandchild in child:
                    files_dict[child.tag].append(str(pathlib.Path(path_from_top,grandchild.attrib['path_name'])))
        return files_dict

    def get_outputs(self):
        """get relative path to output files"""

        res_dir=self.numodis_parameters.relative_path.save_dir
        self.numodis_parameters['output_path']=AttributeDict()

        self.numodis_parameters.output_path["graphs"]=[]
        self.numodis_parameters.output_path["defects"]=[]
        # graphs
        graphs_ET_list=self.numodis_parameters.graphs
        for graph_ET in graphs_ET_list:
            for child in graph_ET:
                for grandchild in child:
                    if grandchild.tag=="lammps":
                        graph_path=str(pathlib.Path(res_dir,grandchild.attrib["name"]))
                        defects_path=str(pathlib.Path(res_dir,grandchild.attrib["defects"]))
                        self.numodis_parameters.output_path.graphs.append(graph_path) # atoms.data
                        self.numodis_parameters.output_path.defects.append(defects_path) # defects.vtk

    def get_graph(self):
        """ Read graph.xml file and store relevant information.
        """
        if 'graphs' in self.numodis_parameters.relative_path:
            self.numodis_parameters['graphs']=[]
            for graph_path in self.numodis_parameters.relative_path.graphs:
                xmlstring=self.get_object_content(graph_path)
                self.numodis_parameters.graphs.append(ET.fromstring(xmlstring))

    def get_inputdata(self):
        """ Read donnees.xml file and store relevant information.
        """
        if 'inputdata' in self.numodis_parameters.relative_path:
            data_path=self.numodis_parameters.relative_path.inputdata
            xmlstring=self.get_object_content(data_path)
            self.numodis_parameters['inputdata']=ET.fromstring(xmlstring)

    def get_materials(self):
        """ Read Fe.xml file and store relevant information.
        """
        if 'materials' in self.numodis_parameters.relative_path:
            self.numodis_parameters['materials']=[]
            for material_path in self.numodis_parameters.relative_path.materials:
                xmlstring=self.get_object_content(material_path)
                self.numodis_parameters.materials.append(ET.fromstring(xmlstring))
    
    @property
    def data_file_path(self):
        return self.base.attributes.get("datafile_path")