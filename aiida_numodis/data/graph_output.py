"""
Module for storing LAMMPS data files created by Numodis.

See information on LAMMPS Data Format: https://docs.lammps.org/2001/data_format.html
"""

from aiida.orm import Data
import logging
import pandas as pd
import tempfile
import io

class LammpsStructureFiles(Data):
    """
   
    """

    def __init__(self):
        """Stores a dictionary with all output files
        "atoms":SinglefileData()        
        """
        
        logger=logging.getLogger(__name__)  # log
        super().__init__()

    def add_struct_file(self,handle, key='atoms.data'):
        """
        TODO: improve parsing.

        :param handle: structure file, typically atoms.data
        :type handle: File like object
        """
        logger=logging.getLogger(__name__)  # log
        
        # Read text file as pandas dataframe
        df=pd.read_csv(handle,sep='\s+', header=None, skip_blank_lines=False, low_memory=False)

        # store dataFrame as hdf5
        with pd.HDFStore(
            "in-memory-save-file",
            mode="w",
            driver="H5FD_CORE",
            driver_core_backing_store=0,
                ) as store:
            store.put("atoms", df, format="table")
            binary_data = store._handle.get_file_image()
        byte_stream=io.BytesIO(binary_data)
        
        # store hdf5 file
        self.base.repository.put_object_from_filelike(byte_stream,key)

    def get_file(self,key="atoms.data"):
        """
        read stored hdf5 file and return text file.
        """
        # get stored hdf5 file
        with self.base.repository.open(key,'rb') as h5file:
            content=h5file.read()

        # read hdf5 file with pandas
        with pd.HDFStore(
            "data.h5",
            mode="r",
            driver="H5FD_CORE",
            driver_core_backing_store=0,
            driver_core_image=content
        ) as h5file:
            df=pd.read_hdf(h5file,key='atoms',mode='r')

        # write pandas dataframe to txt file
        temp_handle=tempfile.NamedTemporaryFile()
        df.to_csv(temp_handle, sep=' ',header=False, index=False)
        temp_handle.flush()
        temp_handle.seek(0)
        return temp_handle