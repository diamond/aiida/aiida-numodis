"""
Utilitaries module for path manipulation and reading numodis xml input files.
"""

import pathlib
import logging
import xml.etree.ElementTree as ET

class PurePath(type(pathlib.PurePath())):
    """
    Overloading pathlib.PurePath class with useful methods to wrap paths into folder. Creating a subclass would be better but is tricky due to PurePath implementation in pathlib.
    It is assumed that path only use directories and has no filename.
    """
    
    # path to first highest directory, is computed in top_dir_level()
    path_to_top='.'
    
    # Number of arbitrarily-named directories when computing reverse path.
    n_dir=0
    def top_dir_level(self):
        """
        Computes the depth difference between the current directory '.' and the highest level directory.
        
        :rtype: int
        :returns: depth difference with the higest directory, always positive.
        
        Examples
        --------
        >>> p=PurePath('.')
        >>> p.top_dir_level()
        0
        >>> p=PurePath('..')
        >>> p.top_dir_level()
        1
        >>> p=PurePath('dir1')
        >>> p.top_dir_level()
        0
        >>> p=PurePath('./dir1/../../../dir2')
        >>> p.top_dir_level()
        2
        """
        dir_list=self.parts
        level=0
        max_level=0
        tmp_path='.'
        
        for i,directory in enumerate(dir_list):
            tmp_path=PurePath(tmp_path,directory)
            if directory=='..':
                level+=1
            else:
                level-=1
            if level>max_level:
                max_level=level
                self.path_to_top=tmp_path
        return max_level

    def reverse_path(self,id_0=0):
        """
        Computes an arbitrary path from top directory to origin directory.
        
        :param id_0: initial id for naming arbitrary directories.
        :type id_0: int
        
        :rtype: PurePath
        :returns: Path from top directory to origin directory
        
        Examples
        --------
        >>> p = PurePath('./dir1/../../../dir2')
        >>> print(p.reverse_path())
        dir_0/dir_1
        """
        self.n_dir=0
        reverse_path='.'
        top_level=self.top_dir_level()
        for i in range(id_0,id_0+top_level):
            reverse_path=PurePath(reverse_path,'dir_'+str(i))
            self.n_dir+=1
        return PurePath(reverse_path)


def get_path_from_top(relative_path_list):
    """
    Computes arbitrary path from top directory to origin directory. Only xml are considered files, others are considered directories.
    
    :param relative_path_list: List of relative paths from origin directory where data_files.xml is located to input xml files.
    :type relative_path_list: str list
    :rtype: PurePath
    :returns: Path from top directory to origin directory.
    
    Examples
    --------
    >>> path_list = ['res', 'topo.xml', 'micro.xml', '../unusual_location/donnees.xml', 'graph.xml', 'Fe.xml']
    >>> print(get_path_from_top(path_list))
    dir_0
    
    >>> path_list = ['res', 'topo.xml', 'micro.xml', '../unusual_location/../../../donnees.xml', 'graph.xml', 'Fe.xml']
    >>> print(get_path_from_top(path_list))
    dir_0/dir_1/dir_2
    """
    path_from_top='.'
    top_level=0
    for path_with_file in relative_path_list:
        if path_with_file[-4:]=='.xml':
            path_nofile = pathlib.Path(path_with_file).parent
        else:
            path_nofile = pathlib.Path(path_with_file)
        mypure_path=PurePath(path_nofile)
        if mypure_path.top_dir_level() > top_level:
            top_level=mypure_path.top_dir_level()
            path_from_top=mypure_path.path_to_top.reverse_path()
    return path_from_top

def read_data_xml_file(filename):
    """
    Reads data_files.xml to extract relative paths of input files which need to be copied by AiiDA.
    
    :param filename: path to data_files.xml.
    :type filename: str
    
    :rtype: list
    :returns: 
    
        - List of file paths listed in data_files.xml.

        - Result directory.
    
    Examples
    --------    
    >>> read_data_xml_file('../../examples/data/data_files.xml')
    ['res', 'topo.xml', 'micro.xml', '../unusual_location/donnees.xml', 'graph.xml', 'Fe.xml']
    """
    files_list=[]
    tree = ET.parse(filename)
    root = tree.getroot()
    for child in root:
        if child.attrib:
            files_list.append(child.attrib['path_name'])
        else:
            for grandchild in child:
                files_list.append(grandchild.attrib['path_name'])
  
    return files_list

def get_target_list(path_from_top, relative_path_list):
    """
    Computes relative paths with .xml files in the target directory. Relative paths with no .xml files are ignored.
    
    :param path_from_top: Arbitrary path from top to origin directory.
    :type path_from_top: PurePath list
    :param relative_path_list: List of relative paths from origin directory where data_files.xml is located to input xml files.
    :type relative_path_list: str list
    :rtype: Path list
    :returns: List of target paths from top directory to .xml file.
    
    Examples
    --------
    >>> path_list = ['res', 'topo.xml', 'micro.xml', '../unusual_location/donnees.xml', 'graph.xml', 'Fe.xml']
    >>> from_top = get_path_from_top(path_list)
    >>> [str(path) for path in get_target_list(from_top, path_list)]
    ['dir_0/data_files.xml', 'dir_0/topo.xml', 'dir_0/micro.xml', 'dir_0/../unusual_location/donnees.xml', 'dir_0/graph.xml', 'dir_0/Fe.xml']
    
    >>> path_list = ['res', 'topo.xml', '../unusual_location/../../../donnees.xml']
    >>> from_top = get_path_from_top(path_list)
    >>> [str(path) for path in get_target_list(from_top, path_list)]
    ['dir_0/dir_1/dir_2/data_files.xml', 'dir_0/dir_1/dir_2/topo.xml', 'dir_0/dir_1/dir_2/../unusual_location/../../../donnees.xml']
    """
    target_list=[pathlib.Path(path_from_top,'data_files.xml')]
    for path_with_file in relative_path_list:
        if path_with_file[-4:]=='.xml':   # exclude non .xml files and directories
            target_list.append(pathlib.Path(path_from_top,path_with_file))
    return target_list

def get_source_list(file_path,relative_path_list):
    """
    Computes absolute paths to .xml input files.
    
    :param file_path: path to data_files.xml input file.
    :type file_path: str
    :param relative_path_list: List of relative paths from origin directory where data_files.xml is located to input xml files.
    :type relative_path_list: str list
    :rtype: Path list
    :returns: List of paths to .xml input files.    
    """
    
    logger=logging.getLogger(__name__)
    source_list=[file_path]
    dir_path=pathlib.Path(file_path).parents[0]
    for path in relative_path_list:
        if path[-4:]=='.xml':   # exclude non .xml files and directories
            source_list.append(dir_path.resolve() / path)
            logger.debug('file : '+str(source_list[-1])) # log
    return source_list

def get_source_and_target(file_path):
    """
    Computes
    
    :param file_path: path to data_files.xml input file.
    :type file_path: str
    :rtype: path list
    :returns:
        
        - source_list: List of paths to .xml input files.
        
        - target_list: List of target paths from top directory to .xml file.
    """
    
    # read input files relative paths
    relative_path_list = read_data_xml_file(file_path)
    
    # find path from top
    path_from_top=get_path_from_top(relative_path_list)
    
    # target locations
    target_list=get_target_list(path_from_top,relative_path_list)
    #results_directory=pathlib.Path(path_from_top,results_relative_directory)

    # convert relative to absolute paths
    source_list=get_source_list(file_path,relative_path_list)
    
    return source_list, target_list