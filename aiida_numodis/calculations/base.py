#! /usr/bin/env python
"""
AiiDA plugin for ``NUMODIS``.

"""

import logging

from aiida.common import datastructures
from aiida.engine import CalcJob
from aiida.orm import SinglefileData, FolderData

class NumodisBaseCalculation(CalcJob):
    """
    A basic plugin for performing calculations in ``NUMODIS`` using aiida.
    
    use some inputs to create numodis input files.
    """

    @classmethod
    def define(cls, spec):
        """Define inputs and outputs of the calculation.
        
        :param cls: 
        :type cls: 
        :param spec: 
        :type spec: 
        
        :returns: Nothing.
        """
        #yapf: disable
        super(NumodisRawCalculation, cls).define(spec)
        
        # new ports
        spec.input('data_files', valid_type=FolderData, help='Data directory including data_files.xml and all other relevant files. You may want to use NumodisXmlData("data_files.xml").')
        spec.output('result', valid_type=SinglefileData, help='numodis log.')
        
        spec.input('metadata.options.output_filename', valid_type=str, default='patch.numodis')
        spec.inputs['metadata']['options']['resources'].default = {
                                            'num_machines': 1,
                                            'num_mpiprocs_per_machine': 1,
                                            }
        spec.inputs['metadata']['options']['parser_name'].default = 'numodis'
        
        spec.exit_code(300, 'ERROR_MISSING_OUTPUT_FILES',
            message='Calculation did not produce all expected output files.')
            
    def prepare_for_submission(self, folder):
        """
        Create input files.
        
        :param folder: an 'aiida.common.folders.Folder' where the plugin should temporarily place all files needed by the calculation.
        :type folder: directory
        :return: 'aiida.common.datastructures.CalcInfo' instance
        """
        logger=logging.getLogger(__name__)
        
        codeinfo=datastructures.CodeInfo()
        codeinfo.cmdline_params = ['-f','data_files.xml'] #self.inputs.data_files.filename
        codeinfo.code_uuid=self.inputs.code.uuid
        codeinfo.stdout_name = self.metadata.options.output_filename
        
        # Prepare a 'CalcInfo' to be returned to the engine
        calcinfo = datastructures.CalcInfo()
        calcinfo.codes_info = [codeinfo]
        
        calcinfo.local_copy_list = []
        logger.debug("Reading input folder")
        for file_obj in self.inputs.data_files.list_objects():
            filename=file_obj.name
            logger.debug('found file : '+filename)
            calcinfo.local_copy_list.append((self.inputs.data_files.uuid,filename,filename))
        
        calcinfo.retrieve_list = [self.metadata.options.output_filename]
        
        return calcinfo