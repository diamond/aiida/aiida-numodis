#! /usr/bin/env python
"""
AiiDA plugin for ``NUMODIS``.


TODO: add validation of input data


Exit codes:

  0  The process finished successfully.
  1  The process has failed with an unspecified error.
  2  The process failed with legacy failure mode.
 10  The process returned an invalid output.
 11  The process did not register a required output.
100  The process did not have the required `retrieved` output.
110  The job ran out of memory.
120  The job ran out of walltime.
131  The specified account is invalid.
140  The node running the job failed.
150  {message}
300  Calculation did not produce all expected output files.

Exit codes that invalidate the cache are marked in bold red.


"""

import logging

from aiida_numodis.calculations.functions import rawnumodis2lammps
from aiida_numodis.data import NumodisXmlData
from aiida.common import datastructures
from aiida.engine import CalcJob
from aiida.orm import SinglefileData, Dict
from aiida_numodis.data.graph_output import LammpsStructureFiles
from aiida_numodis.data.vtk_output import NumodisVtkFiles


class NumodisRawCalculation(CalcJob):
    """
    A basic plugin for performing calculations in ``NUMODIS`` using aiida.
    The plugin will take the usual ``NUMODIS`` .xml input files as inputs.
    """

    @classmethod
    def define(cls, spec):
        """Define inputs and outputs of the calculation.
        
        """
        super(NumodisRawCalculation, cls).define(spec)
        
        # new ports
        spec.input('data_files', valid_type=NumodisXmlData, help='Data directory including data_files.xml and all other relevant files. You may want to use NumodisXmlData("data_files.xml").')
        spec.input('lammps_data', valid_type=SinglefileData, required=False, help='LAMMPS dump file, LammpsTrajectory class')
        
        spec.output('log', valid_type=SinglefileData, help='numodis log.')
        spec.output('structures', valid_type=LammpsStructureFiles, required=False, help='.data structure files')
        spec.output('vtk_files', valid_type=NumodisVtkFiles, required=False, help='.vtk files')
        
        spec.input('metadata.options.output_filename', valid_type=str, default='numodis.log')
        spec.inputs['metadata']['options']['resources'].default = {
                                            'num_machines': 1,
                                            'num_mpiprocs_per_machine': 1,
                                            }
        spec.inputs['metadata']['options']['parser_name'].default = 'numodis'
        spec.inputs['metadata']['label'].default = 'test label 42'

        spec.exit_code(300, 'ERROR_MISSING_OUTPUT_FILES',
            message='Calculation did not produce all expected output files.')
            
    def prepare_for_submission(self, folder):
        """
        Create input files.
        
        :param folder: an 'aiida.common.folders.Folder' where the plugin should temporarily place all files needed by the calculation.
        :type folder: directory
        :return: 'aiida.common.datastructures.CalcInfo' instance
        """
        logger=logging.getLogger(__name__)
        # get relative paths of input
        datafiles_path=self.inputs.data_files.base.attributes.get("data_file_path")
        self.inputs.data_files.read_data()

        codeinfo=datastructures.CodeInfo()
        # command line
        codeinfo.cmdline_params = ['-f',datafiles_path]
        codeinfo.code_uuid=self.inputs.code.uuid
        codeinfo.stdout_name = self.metadata.options.output_filename
        
        # Prepare a 'CalcInfo' to be returned to the engine
        calcinfo = datastructures.CalcInfo()
        calcinfo.codes_info = [codeinfo]
        
        # copy .xml input files
        calcinfo.local_copy_list = []
        logger.debug("Reading input folder")
        for file_obj in self.inputs.data_files.list_objects():
            filename=file_obj.name
            logger.debug('found file : '+filename)
            calcinfo.local_copy_list.append((self.inputs.data_files.uuid,filename,filename))
        
        # copy optional data from lammps (atoms.data)
        if 'lammps_data' in self.inputs:
            filename=self.inputs.lammps_data.filename
            logger.debug('found file : '+filename)
            calcinfo.local_copy_list.append((self.inputs.lammps_data.uuid,filename,filename))
        
        # List of retrieved files
        # log file (numodis.log)
        calcinfo.retrieve_list = [self.metadata.options.output_filename]

        # structure (atoms.data)
        calcinfo.retrieve_list.extend(self.inputs.data_files.numodis_parameters.output_path.graphs)
        
        # defects (defects.vtk)
        calcinfo.retrieve_list.extend(self.inputs.data_files.numodis_parameters.output_path.defects)
        
        return calcinfo