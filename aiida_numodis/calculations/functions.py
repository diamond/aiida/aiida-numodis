#! /usr/bin/env python
"""
Path operations
"""

import logging
from aiida.orm import Dict
from aiida.common import AttributeDict

def write_control():
    control_dict=AttributeDict()
    # Types of units to be used in the calculation
    control_dict.units = "metal"
    # Size of the time step in the units previously defined
    control_dict.timestep = 0.025
    return control_dict

def write_compute():
    compute_dict={
        "pe": [{"type": [{"keyword": " ", "value": " "}], "group": "all"}],
        "pe/atom": [{"type": [{"keyword": " ", "value": " "}], "group": "all"}],
    }
    return compute_dict

def write_dump():
    dump_dict={"dump_rate": 200}
    return dump_dict

def write_structure():
    structure_dict={"atom_style":"atomic","dimension":3}
    return structure_dict

def write_thermo():
    thermo_dict={
        "printing_rate": 100,
        "thermo_printing": {
#            "v_peatom": True,
            "press": True,
            "lx": True,
            "ly": True,
            "lz": True,
            "etotal": False,
        },
    }
    return thermo_dict

def write_minimize():
    minimize_dict={
    'style':'quickmin',
    'energy_tolerance':1e-20,
    'force_tolerance':1e-30,
    'max_iterations':500,
    'max_evaluations':100000
    }

    return minimize_dict

def write_potential():
    potential_dict={}
    return potential_dict

def rawnumodis2lammps(filepath):
    """
    read numodis input files to create lammps.base input files.
    """
    logger=logging.getLogger(__name__)
    logger.debug(filepath)
    
    _parameters = AttributeDict()
    # Control section specifying global simulation parameters
    _parameters.control=write_control()
    # Set of computes to be evaluated during the calculation
    _parameters.compute = write_compute()
    
    # Control how often the computes are printed to file
    _parameters.dump = write_dump()
    # Parameters used to pass special information about the structure
    #_parameters.structure = AttributeDict()
    _parameters.structure=write_structure()

    # Parameters controlling the global values written directly to the output
    _parameters.thermo = write_thermo()

    _parameters.minimize= write_minimize()   
    
    _parameters.potential = write_potential() 

    parameters=Dict(dict=_parameters)
    return parameters

if __name__ == "__main__":
    import doctest
    doctest.testmod() # unitary tests
