#! usr/bin/env python
"""
###########################

        OBSOLETE

###########################

minimal script to run a NUMODIS calculation using the 'numodis.base' AiiDA plugin.

This script configures a code and computer with aiida_numodis using aiida's python API. If they are already configured on your system you may want to use orm.load_code() or orm.load_computer() instead.
It also requires a set of numodis .xml input files for which examples are provided with aiida_numodis plugin.

For documentation on how to load a code on Aiida please refer to the AiiDA documentation or check launch_numodis_raw_script.py.
AiiDA documentation website: https://aiida.readthedocs.io/projects/aiida-core/en/latest/howto/run_codes.html
"""
from pathlib import Path
from aiida import engine, orm
import os

def run_edge0():
    # full path of the input file. Must be on this local machine.
    input_path = Path(__file__).resolve().parent / 'data/data_files.xml'

    # load code
    computer = orm.load_computer('localhost')
    code = orm.InstalledCode(label='numodis', computer=computer , filepath_executable=os.environ["LOCAL_HOME"]+'/aiida/numodis/NUMODIS/build/numodis.exe', default_calc_job_plugin='numodis.raw')

    # configure builder
    builder = code.get_builder()
    builder.data_files = builder.process_class.num_data(input_path)
    
    # Run the calculation & parse results
    result = engine.run(builder)
    computed_numodis = result['result'].get_content()
    print(f'Computed numodis :\n{computed_numodis}')
    
if __name__="__main__":
    run_edge0()

