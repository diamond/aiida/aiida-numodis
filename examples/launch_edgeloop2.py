#! usr/bin/env python
"""
minimal script to test edgeloop2 workflow, using numodis and lammps.
"""

from aiida.plugins import WorkflowFactory
from aiida.engine import run, submit
from aiida import orm
from aiida import load_profile
from aiida.orm import Str, SinglefileData, Dict
load_profile()

edgeloop2=WorkflowFactory('edgeloop')

# Load configured codes
code_numodis = orm.load_code('numodis@localhost')
code_lammps = orm.load_code('lammps@localhost')
#code_lammps = orm.load_code('lammps@gatsby')



# input files location
script_dir = '/aiida-numodis/examples/'
input_data0 = Str(script_dir + 'data/DD.0/data_files.xml')
input_data2 = Str(script_dir + 'data/DD.2/data_files.xml')
input_data5 = Str(script_dir + 'data/DD.5/data_files.xml')
lammps_potential=Str(script_dir + 'data/MD.1/Fe_mm.eam.fs')
workflow_parameters = SinglefileData(file=script_dir + 'data/workflow_parameters.yml')

replacement_dict={
'DD0':14,
'MD1':26,
'DD2':33,
}

inputs={
    'data_file0':input_data0,
    'data_file2':input_data2,
    'data_file5':input_data5,
    'code_numodis': code_numodis,
    'code_lammps': code_lammps,
    'lammps_potential': lammps_potential,
    'workflow_parameters':workflow_parameters,
#    'replacement_nodes':Dict(replacement_dict),
}

# Submit workflow
workflow_node=submit(edgeloop2, **inputs)
