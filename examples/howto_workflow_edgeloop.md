# How to run Edgeloop workflow
How to run edgeloop workflow using a [Diamond](https://diamond-diadem.github.io/) container.

## Prerequisities :

- Linux computer to run the container on.
- NUMODIS code installed on a computer.
- LAMMPS code installed on a computer.

## 1. Install Apptainer

If Apptainer is not installed on your machine yet, please follow the tutorial on [How to install Apptainer?](https://diamond-diadem.github.io/en/documentation/install-apptainer/howto/)

## 2. Download the container image

Using Apptainer you can then download the Diamond image for the container including AiiDA and the current plugin with the command:

```
apptainer pull oras://gricad-registry.univ-grenoble-alpes.fr/diamond/aiida/aiida2apptainer/aiida_numodis.sif:latest
```

To learn more about apptainer images and how to enter the container see: [How to interact with an Apptainer image ?](https://diamond-diadem.github.io/en/documentation/use-apptainer-image/howto/)

## 3. Configure the code and computer

TODO

## 4. Chose input data

This workflow
For full information on the workflow input data you can run the following command:

```
verdi plugin list aiida.workflows edgeloop
```


