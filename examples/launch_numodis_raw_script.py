#! /usr/bin/env python
"""
minimal script to run a NUMODIS calculation using the 'numodis.raw' AiiDA plugin.

This script assumes an InstalledCode 'numodis@localhost' has been configured already with the 'numodis.raw' plugin.
It also requires a set of numodis .xml input files for which examples are provided with aiida_numodis plugin.

For documentation on how to configure a code on Aiida please refer to the AiiDA documentation or check launch_numodis_base_script.py.
AiiDA documentation website: https://aiida.readthedocs.io/projects/aiida-core/en/latest/howto/run_codes.html
"""
from pathlib import Path
from aiida import engine, orm, load_profile
import os

from aiida_numodis.utils import init_logs
from aiida_numodis.data import NumodisXmlData
import logging
init_logs(level='DEBUG')
logger=logging.getLogger("aiida_numodis")

# Necessary when launching script with python instead of verdi run
from aiida import load_profile
load_profile()

# full path of the input file. Must be on this local machine.
input_path = Path(__file__).resolve().parent / 'data/edge0/data_files.xml'

# load code
try:    # in case code is already configured.
    code = orm.load_code('numodis@localhost')
except: # in case code is not configured yet or multiple instances already exist.
    computer = orm.load_computer('localhost')
    code = orm.InstalledCode(label='numodis', computer=computer , filepath_executable=os.environ["LOCAL_HOME"]+'/aiida/numodis/NUMODIS/build/numodis.exe', default_calc_job_plugin='numodis.raw')

# configure builder
builder = code.get_builder()
builder.data_files = NumodisXmlData(input_path)

# Run the calculation & parse results
result = engine.run(builder)
computed_numodis = result['log'].get_content()
logger.info(f'Computed numodis :\n{computed_numodis}')

struct_numodis = result['structures'].get_file('atoms.data').readlines()[:20]
logger.debug(f'Structure :\n{struct_numodis}')


target=open('test.vtk','wb')
vtk_content=result['vtk_files'].get_file('defects.vtk')
target.write(vtk_content)
target.close()
