#!/bin/bash
#PBS -S /bin/bash
#PBS -l nodes=1:ppn=16
#PBS -M laurent.dupuy@cea.fr
#PBS -m eb

# #cd $PBS_O_WORKDIR
pwd
cp ../edge.0/res/atoms.data .

# module load impi

#----------------
# list of params
#----------------

T=300.0 # temperature [Kelvin]

fdata=atoms.data # input file
fpot=Fe_mm.eam.fs # eam potential
emass=55.845 # atomic mass g/mol
ename=Fe # element name

resdir=res # output directory

teq=5 # equilibration time [ps]

save_restart=1000 # write a restart file every save_restart steps

#---------------------------
# create output directories
#---------------------------
mkdir -p ${resdir}
mkdir -p ${resdir}/restart

#-----------------
# RUN calculation
#-----------------
mpirun -np 16 lmp_mpi \
-var T $T \
-var fdata $fdata \
-var fpot $fpot \
-var emass $emass \
-var ename $ename \
-var res $resdir \
-var teq $teq \
-in  in.data.equilibrate \
-log $resdir/log.${fdata}
