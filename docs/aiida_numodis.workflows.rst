Workflows module
================

:mod:`workflows`
----------------

.. automodule:: aiida_numodis.workflows
    :members:

:mod:`edgeloop2`
-----------------

.. automodule:: aiida_numodis.workflows.edgeloop2
   :members:

