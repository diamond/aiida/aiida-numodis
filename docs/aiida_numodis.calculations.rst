Calculations Module
====================

:mod:`calculations`
---------------------------

.. automodule:: aiida_numodis.calculations
    :members:

:mod:`raw`
-----------------

.. automodule:: aiida_numodis.calculations.raw
    :members:

:mod:`base`
------------------

.. automodule:: aiida_numodis.calculations.base
    :members:

:mod:`functions`
-----------------------

.. automodule:: aiida_numodis.calculations.functions
    :members:


