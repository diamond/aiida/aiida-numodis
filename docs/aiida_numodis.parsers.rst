Parsers module
==============

:mod:`Parsers`
--------------

.. automodule:: aiida_numodis.parsers
    :members:

:mod:`parser`
--------------

.. automodule:: aiida_numodis.parsers.parser
    :members:
