aiida_numodis Package
=====================

:mod:`aiida_numodis`
----------------------------

.. automodule:: aiida_numodis.__init__
    :members:


Modules
-------

.. toctree::

   aiida_numodis.calculations
   aiida_numodis.data
   aiida_numodis.parsers
   aiida_numodis.workflows



:mod:`utils`
-------------------

.. automodule:: aiida_numodis.utils
    :members:



