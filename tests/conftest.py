import sys

def pytest_addoption(parser):
    parser.addoption("--directory", action="store", default="/aiida-numodis/examples", help="path to example directory")
    
def pytest_generate_tests(metafunc):
    sys.path.append(metafunc.config.option.directory)
