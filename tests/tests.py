#! /usr/bin/env python
"""
Module to test the plugin using pytest
"""

from aiida.engine.processes import control
from plumpy import ProcessState
import time


def test_workflow():
    """Test that the edgeloop workflow finishes in a reasonable time."""
    
    timeout=600
    frequency=5
    from launch_edgeloop2 import workflow_node
    i=0
    status=ProcessState.WAITING
    while i*frequency<timeout and (status==ProcessState.WAITING or status==ProcessState.RUNNING):
        time.sleep(frequency)
        i+=1
        status=workflow_node.process_state
    # kill workflow process
    control.kill_processes([workflow_node])
    assert status == ProcessState.FINISHED



