
# Aiida-numodis

An [AiiDA](http://aiida-core.readthedocs.io/) plugin for the ``NUMODIS`` code.

This plugin contains 2 types of calculations:

- `numodis.raw`: Calculation making use of usual NUMODIS input files (`data_files.xml`).
- `numodis.base`: Calculation making use of parameter based input. (TODO)

It also includes 1 workflow:
- `edgeloop2`: 6 steps workflow using Numodis and [LAMMPS](https://www.lammps.org/).

## Installation

```shell
pip install aiida-numodis
```

## Documentation

To compile the plugin documentation first make sure to meet the following prerequisites:

``` shell
pip install sphinx
pip install sphinx_rtd_theme
pip install aiida
```

Then compile in the `docs/` directory:

```shell
cd docs
sphinx-apidoc -o . ../aiida_numodis
make html
```

finally, open the `index.html` file using your preferred browser:

```shell
firefox _build/html/index.html
```

## Usage

### Python API

Example scripts are provided in the `examples/` directory to use the plugin with the python API.

### Command Line Interface

The plugin adds some commands to verdi to manipulate NUMODIS data. Explore them with:
```
verdi data --help
```


## Development

Contribution is very welcomed.

TODO: add proper CI to test the plugin.

Master branch examples should work without any modification in a diamond container environment using absolute paths.
This assumes that the plugin is located in `/aiida-numodis`, in the root directory.

To test the plugin in a different environment, please create a dedicated branch.

## License

MIT
